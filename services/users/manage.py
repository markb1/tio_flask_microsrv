# services/users/manage.py

import os
from flask.cli import FlaskGroup
import coverage

from project import create_app, db
from project.api.models import User


DIR = os.path.abspath(os.path.dirname(__file__))
PROJECT_ROOT = os.path.join(DIR, 'project')
TEST_PATH = os.path.join(PROJECT_ROOT, 'tests')

COV = coverage.coverage(
    branch=True,
    include='project/*',
    omit=[
        'project/tests/*',
        'project/config.py',
    ]
)
COV.start()


cli = FlaskGroup(create_app=create_app)


@cli.command('recreate_db')
def recreate_db():
    """ sqlalchemy 'db' object
    """
    db.drop_all()
    db.create_all()
    db.session.commit()


@cli.command()
def test():
    """ run Pytest tests
    """
    import pytest
    rv = pytest.main([TEST_PATH, '--verbose'])

    COV.stop()
    COV.save()
    print('Code-coverage summary:')
    COV.report()
    COV.html_report()
    # COV.erase()

    exit(rv)


@cli.command('seed_db')
def seed_db():
    """ add some Users
    """
    db.session.add(
        User(username='mbiggers', email="mrbiggers@yahoo.com",
             active=True)
    )
    db.session.add(
        User(username='mherman', email="michael@mherman.org",
             active=True)
    )
    db.session.commit()


if __name__ == '__main__':
    cli()
