# services/users/project/__init__.py

# REF: https://flask-restful.readthedocs.io/en/latest/quickstart.html

import os

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from .config import config

# instantiate the db
db = SQLAlchemy()


def create_app(script_info=None):
    """ an Application Factory, used to create 'user' service
        REF: http://flask.pocoo.org/docs/1.1.x/patterns/appfactories/
    """

    # instantiate the app
    app = Flask(__name__)

    # set config
    flask_env = os.getenv('FLASK_ENV')
    app.config.from_object(config[flask_env])

    if flask_env == 'development':
        from pprint import pprint
        import sys
        pprint(app.config, stream=sys.stderr, indent=2)

    # Set up extensions
    db.init_app(app)

    # Register blueprints
    from project.api.users import users_blueprint
    app.register_blueprint(users_blueprint)

    # shell context for flask cli
    @app.shell_context_processor
    def ctx():
        return {'app': app, 'db': db}

    return app
