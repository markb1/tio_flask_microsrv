# services/users/project/api/users.py

from sqlalchemy import exc
from flask import Blueprint, request, render_template, current_app
from flask_restful import Resource, Api

from project import db
from .models import User

users_blueprint = Blueprint('users', __name__,
                            template_folder='./templates')


@users_blueprint.route('/', methods=['GET', 'POST'])
def index():
    """ """
    if request.method == 'POST':
        username = request.form['username']
        email = request.form['email']
        db.session.add(User(username=username, email=email, active=True))
        db.session.commit()

    users = User.query.all()
    return render_template('index.html', users=users)


class UsersPing(Resource):
    def get(self):
        return {
            'status': 'success',
            'message': 'pong!'
        }


class UsersOne(Resource):
    def get(self, user_id):
        """ Get a single-user details """
        fail_object = {
            'status': 'fail',
            'message': 'User does not exist'
        }
        try:
            user = User.query.filter_by(id=int(user_id)).first()
            if not user:
                return fail_object, 404
            else:
                response_object = {
                    'status': 'success',
                    'data': {
                        'id': user.id,
                        'username': user.username,
                        'email': user.email,
                        'active': user.active
                    }
                }
                return response_object, 200
        except ValueError as e:
            fail_object['message'] = str(e)
            return fail_object, 404


class UsersAll(Resource):
    def get(self):
        """Get all users"""
        users = [user.to_json() for user in User.query.all()]
        current_app.logger.info("Users found:", users)

        response_object = {
            'status': 'success',
            'data': {
                'users': users
            }
        }
        return response_object, 200


class UsersList(Resource):
    """ """
    def get(self):
        """ Get all users """
        response_object = {
            'status': 'success',
            'data': {
                'users': [user.to_json() for user in User.query.all()]
            }
        }
        return response_object, 200

    def post(self):
        """ add 1 - many users """
        post_data = request.get_json()

        response_object = {
            'status': 'fail',
            'message': 'Invalid payload.'
        }
        if not post_data:
            return response_object, 400

        username = post_data.get('username')
        email = post_data.get('email')

        try:
            user = User.query.filter_by(email=email).first()

            if not user:  # add a new-user, by 'email'
                db.session.add(
                    User(username=username, email=email, active=True)
                )
                db.session.commit()
                response_object['status'] = 'success'
                response_object['message'] = f'<{email}> user added!'
                return response_object, 201
            else:
                response_object['message'] = \
                    'Sorry. That email already exists.'
                return response_object, 400

        except exc.IntegrityError as e:
            response_object = {
                'status': 'fail',
                'message': str(e),
            }
            db.session.rollback()
            return response_object, 400


api = Api(users_blueprint)

api.add_resource(UsersOne, '/users/<int:user_id>')
api.add_resource(UsersPing, '/users/ping')
api.add_resource(UsersList, '/users')
