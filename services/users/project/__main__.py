# services/users/project/__main__.py

from pprint import pformat
from project import create_app


app = create_app()
app.logger.info(
    pformat(app.config)
)
app.run()
