# services/users/project/config.py

import os
import logging


class BaseConfig(object):
    """Base configuration"""
    DEBUG = True
    TESTING = False
    SQLALCHEMY_TRACK_MODIFICATIONS = False  # new
    SECRET_KEY = 'my_precious'  # new


class DevelopmentConfig(BaseConfig):
    """Development configuration"""
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL')
    LOG_LEVEL = logging.DEBUG


class TestingConfig(BaseConfig):
    """Testing configuration"""
    TESTING = True
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_TEST_URL')  # new


class ProductionConfig(BaseConfig):
    """Production configuration"""
    DEBUG = False
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL')  # new
    LOG_LEVEL = logging.INFO


config = {
    'development': DevelopmentConfig,
    'testing': TestingConfig,

    'staging': ProductionConfig,
    'production': ProductionConfig,
    'default': ProductionConfig,
}
