import pytest
# from pprint import pprint

from project import create_app, db as _db
from project.api.models import User

from .factories import (
    UserFactory, reset_userfactory
)


@pytest.fixture
def app():
    """ """
    _app = create_app()
    yield _app


@pytest.fixture(scope='function', autouse=True)
def db(app):
    _db.app = app

    with app.app_context():
        _db.drop_all()
        _db.create_all()

    # db_bootstrap(_db)

    yield _db

    # purposely close DB connection
    _db.session.close()
    _db.drop_all()


@pytest.fixture
def user(db):
    """ An ordinary user...
    """
    _user = UserFactory()
    db.session.commit()
    yield _user

    db.session.delete(_user)
    db.session.commit()
    reset_userfactory()


@pytest.fixture
def admin_user(db):
    _user = User(
        username='admin',
        email='admin@example.com',
        active=True
    )

    db.session.add(_user)
    db.session.commit()

    yield _user

    db.session.delete(_user)
    db.session.commit()


@pytest.fixture(scope='function')
def users_5(db):
    """ create 5 users, persist them
    """
    _users = [UserFactory() for n in range(5)]
    db.session.commit()

    yield _users

    for u in _users[:]:
        db.session.delete(u)

    db.session.commit()
    reset_userfactory()


@pytest.fixture
# def admin_headers(admin_user, client):
def admin_headers(client):
    """ Ref: restful_api_karec/tests/conftest.py
    """
    res = {
        'content-type': 'application/json'
    }
    return res


def db_bootstrap(db):
    """ get the 'user' table going!
    """
    luser = UserFactory()
    # (
    #     username='admin',
    #     email='admin@example.com',
    #     active=True
    # )

    db.session.add(luser)
    db.session.commit()

    db.session.delete(luser)
    db.session.commit()
