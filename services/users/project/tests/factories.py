# -*- coding: utf-8 -*-

# Refs:
#   https://github.com/sloria/cookiecutter-flask.git /
#     {{cookiecutter.app_name}}/tests/factories.py

from factory import (  # noqa: F401
    Sequence,
    PostGenerationMethodCall,  # unused
)
from factory.alchemy import SQLAlchemyModelFactory

from project import db
from project.api.models import User


class BaseFactory(SQLAlchemyModelFactory):
    """ Base factory
    """

    class Meta:
        """ factory configuration
        """
        abstract = True
        sqlalchemy_session = db.session


class UserFactory(BaseFactory):
    """ User factory
    """
    username = Sequence(lambda n: 'user{0}'.format(n+1))
    email = Sequence(lambda n: 'user{0}@example.com'.format(n+1))
    active = True
    # password = PostGenerationMethodCall('set_password', 'example')

    class Meta:
        """ factory configuration
        """
        model = User


def reset_userfactory():
    """ reset Sequence(s) of: username, email
    """
    UserFactory.reset_sequence()
