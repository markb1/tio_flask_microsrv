# services/users/project/tests/api/users/test_user.py

from pprint import pprint


def test_user_ping(client, admin_headers):
    """ """
    # test users_ping
    rep = client.get(f"/users/ping", headers=admin_headers)
    assert rep.status_code == 200

    data = rep.get_json()
    assert 'pong' in data['message']
    assert 'success' in data['status']


def test_get_user(client, db, user, admin_headers):
    """ """
    # test 404
    resp = client.get("/users/9999", headers=admin_headers)
    assert resp.status_code == 404

    # test get_user
    resp = client.get(f"/users/{user.id}", headers=admin_headers)
    assert resp.status_code == 200

    data = resp.get_json()['data']
    assert data['username'] == user.username
    assert data['email'] == user.email
    assert data['active'] == user.active


def test_add_user(client, db, admin_headers):
    """ """
    udata = {
        'username': 'mherman',
        'email': 'michael@mherman.org',
    }
    resp = client.post('/users', json=udata, headers=admin_headers)

    assert resp.status_code == 201

    data = resp.get_json()
    assert 'user added' in data['message']
    assert 'success' in data['status']


def test_add_user_invalid_data(client, db, admin_headers):
    """ invalid "payload", no POST data sent
    """
    resp = client.post('/users', json={}, headers=admin_headers)

    assert resp.status_code == 400

    data = resp.get_json()
    assert 'Invalid payload' in data['message']
    assert 'fail' in data['status']


def test_add_user_invalid_json_keys(client, db, admin_headers):
    """ invalid "payload", faulty POST data "keys"
    """
    udata = {
        'lusers_name': 'mherman',
        'yomail': 'michael@mherman.org',
    }
    resp = client.post('/users', json=udata, headers=admin_headers)

    assert resp.status_code == 400

    data = resp.get_json()
    assert 'NOT NULL constraint failed' or \
        'username" violates not-null constraint' in data['message']
    assert 'fail' in data['status']


def test_add_user_duplicate_email(client, db, admin_headers):
    """
    Adding a user that already exists in the database
    """
    udata = {
        'username': 'mherman',
        'email': 'michael@mherman.org',
    }
    resp = client.post('/users', json=udata, headers=admin_headers)

    assert resp.status_code == 201

    data = resp.get_json()
    assert 'user added' in data['message']
    assert 'success' in data['status']

    resp = client.post('/users', json=udata, headers=admin_headers)

    assert resp.status_code == 400

    data = resp.get_json()
    assert 'email already exists' in data['message']
    assert 'fail' in data['status']


def test_single_user(client, db, user, admin_headers):
    """ add of one user, and verify
    """
    resp = client.get(f"/users/{user.id}", headers=admin_headers)

    assert resp.status_code == 200
    js = resp.get_json()
    data = js['data']

    pprint(data)
    assert 'success' in js['status']
    assert data['username'] == user.username
    assert data['email'] == user.email
    assert data['active'] == user.active
    assert data['id'] == 1


def test_single_user_no_id(client, admin_headers):
    """ test for error if no user-id
    """
    resp = client.get("/users/xyzzy", headers=admin_headers)

    assert resp.status_code == 404

    js = resp.get_json()
    assert js is None


def test_single_user_incorrect_id(client, admin_headers):
    """ test for error if user-id
    """
    resp = client.get("/users/9999999", headers=admin_headers)

    assert resp.status_code == 404
    js = resp.get_json()

    assert 'fail' in js['status']
    assert 'does not exist' in js['message']


def test_all_users(client, db, users_5, admin_headers):
    """ get all users (added), and verify
    """
    resp = client.get('/users', headers=admin_headers)

    assert resp.status_code == 200
    js = resp.get_json()
    data = js['data']
    assert 'success' in js['status']

    pprint(data['users'])
    for n in range(5):
        assert data['users'][n]['username'] == users_5[n].username
        assert data['users'][n]['email'] == users_5[n].email
        assert data['users'][n]['active'] == users_5[n].active
        assert data['users'][n]['id'] == n + 1


def test_main_add_user(client, db):
    """ """
    udata = {
        'username': 'mherman',
        'email': 'michael@mherman.org',
    }
    resp = client.post('/', data=udata, follow_redirects=True)

    assert resp.status_code == 200
    assert '200 OK' in resp.status

    page = resp.data.decode('utf')
    assert 'mherman' in page
    # assert 'All Users' in page


def test_main_with_users(client, db, users_5):
    """ """
    resp = client.get('/')

    assert resp.status_code == 200
    assert '200 OK' in resp.status

    page = resp.data.decode('utf')

    for n in range(5):
        assert 'user{}'.format(n+1) in page
    # assert 'All Users' in page


def test_main_no_users(client, db):
    """ """
    resp = client.get('/')

    assert resp.status_code == 200
    assert '200 OK' in resp.status

    page = resp.data.decode('utf')
    assert 'No users!' in page
    # assert 'All Users' in page
