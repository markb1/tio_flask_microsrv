
# Microservices with Docker, Flask, and React

## Overview
Learn how to build, test, and deploy microservices powered by Docker,
Flask, and React!

In this course, you will learn how to quickly spin up a reproducible
development environment with Docker to manage a number of
microservices. Once the app is up and running locally, you'll learn
how to deploy it to an Amazon EC2 instance. Finally, we'll look at
scaling the services on Amazon Elastic Container Service (ECS) and
adding AWS Lambda.

We'll also practice Test-Driven development (TDD) throughout, writing
tests first when it makes sense to do so. The focus will be on
server-side unit and integration tests, client-side unit tests, and
browser-based end-to-end tests to ensure the entire system works as
expected.

"Code without tests is broken by design." - Jacob Kaplan-Moss

## What will you build?

### Services

* users - server-side Flask app for managing users and auth
* client - client-side React app
* nginx - reverse proxy web server
* swagger - Swagger API docs
* scores - server-side Flask app for managing user scores
* exercises - server-side Flask app for managing exercises

### App

 *microservice architecture* graphic

Check out the live app, running on a cluster of EC2 instances:
http://testdriven-production-alb-1112328201.us-east-1.elb.amazonaws.com

## Tools and Technologies

 - Python
 - Flask
 - Docker
 - Postgres
 - Node and NPM
 - React
 - Cypress
 - Swagger
 - Amazon Web Services (AWS)

## Concepts

 - Microservice Architecture
 - Test-Driven Development (TDD)
 - Continuous Integration (CI)
 - Continuous Delivery (CD)
 - Code Coverage
 - Code Quality
 - Token-based Authentication
 - Containerization
 - Container Orchestration
 - Serverless Architecture

## Course Info

 Current version: 2.5.3
 Last updated: October 7th, 2019
 Author:  Michael Herman

## What will you learn?

### Part 1

In this first part, you'll learn how to quickly spin up a *reproducible
development environment* with Docker to create a *RESTful API* powered by
Python, Postgres, and the Flask web framework. After the app is up and running
locally, you'll learn how to deploy it to an Amazon EC2 instance.

**Tools and Technologies**: Python, Flask, Flask-RESTful, Docker, Postgres,
Flask-SQLAlchemy, Flask-Testing, Gunicorn, Nginx, Bulma CSS

### Part 2

In part 2, we'll add code coverage and continuous integration (CI) testing to
ensure that each service can be run and tested independently from the
whole. Finally, we'll add React along with Jest (a JavaScript test runner) and
Enzyme (a testing library designed specifically for React) to the client-side.

**Tools and Technologies**: Coverage.py, flake8, Flask Debug Toolbar, Node, NPM,
React, Enzyme, Jest, Axios, Flask-CORS

### Part 3

In part 3, we'll add database migrations along with password hashing in order
to implement token-based authentication to the users service with JSON Web
Tokens (JWTs). We'll then turn our attention to the client-side and add React
Router to the React app to enable client-side routing along with client-side
authentication and authorization.

**Tools and Technologies**: Flask-Migrate, Flask-Bcrypt, PyJWT, react-router-dom

### Part 4

In part 4, we'll add end-to-end (e2e) tests with Cypress, form validation to
the React app, and a Swagger service to document the API. We'll also deal with
some tech debt and set up a staging environment to test on before the app goes
into production.

**Tools and Technologies**: Cypress, Swagger UI, OpenAPI


### Part 5 (optional?)

In part 5, we'll dive into container orchestration with Amazon ECS as we move
our staging and production environments to a more scalable infrastructure.
We'll also add Amazon's Elastic Container Registry along with Elastic Load
Balancing for load balancing and Relational Database Service (RDS) for data
persistence.

**Tools and Technologies**: AWS, EC2, Elastic Container Registry (ECR), Elastic
Container Service (ECS), Elastic Load Balancing (ELB), Application Load
Balancer (ALB), Relational Database Service (RDS)


### Part 6

In part 6, we'll focus our attention on adding a new Flask service, with two
RESTful-resources, to evaluate user-submitted code. Along the way, we'll tie
in AWS Lambda and API Gateway and spend a bit of time refactoring React and
the end-to-end test suite. Finally, we'll update the staging and production
environments on ECS.

**Tools and Technologies**: AWS Lambda and API Gateway, React-Ace, and Flask

### Part 7

In part 7, we'll refactor the AWS Lambda function to make it dynamic so it can
be used with more than one exercise, introduce type-checking on the
client-side with React PropTypes, and update a number of components. We'll
also introduce another new Flask service to manage scores. Again, we'll update
the staging and production environments on ECS.

**Tools and Technologies**: AWS Lambda and ECS, PropTypes, and Flask

## Table of Contents

From the course *Microservices with Docker, Flask, and React*

### Part 1

 - Introduction
 - Changelog
 - Microservices
 - App Overview
 - Getting Started
 - Docker Config
 - Postgres Setup
 - Test Setup
 - Flask Blueprints
 - RESTful Routes
 - Deployment
 - Jinja Templates
 - Workflow
 - Structure

### Part 2

 - Introduction
 - Code Coverage and Quality
 - Continuous Integration
 - Flask Debug Toolbar
 - React Setup
 - Testing React
 - React Forms
 - React and Docker
 - Next Steps
 - Structure

### Part 3

 - Introduction
 - Flask Migrate
 - Flask Bcrypt
 - JWT Setup
 - Auth Routes
 - React Router
 - React Bulma
 - React Authentication - part 1
 - Mocking User Interaction
 - React Authentication - part 2
 - Authorization
 - Update Component
 - Update Docker
 - Structure

### Part 4

 - Introduction
 - End-to-End Test Setup
 - End-to-End Test Specs
 - React Component Refactor
 - React Form Validation
 - React Flash Messaging
 - Update Test Script
 - Swagger Setup
 - Staging Environment
 - Production Environment
 - Workflow
 - Structure

### Part 5

 - Introduction
 - Container Orchestration
 - IAM
 - Elastic Container Registry
 - Elastic Load Balancer
 - Elastic Container Service
 - ECS Staging
 - Setting up RDS
 - ECS Production Setup
 - ECS Production Automation
 - Workflow
 - Structure

### Part 6

 - Introduction
 - React Refactor
 - React Ace Code Editor
 - Exercises Service Setup
 - Exercises Database
 - Exercises API
 - Code Evaluation with AWS Lambda
 - Update Exercises Component
 - ECS Deployment - Staging
 - ECS Deployment - Production
 - Autoscaling
 - Workflow
 - Structure

### Part 7

 - Introduction
 - Lambda Refactor
 - Exercise Component
 - AJAX Refactor
 - Type Checking
 - Scores Service
 - Exercises Component Refactor
 - ECS Staging Update
 - E2E Refactor
 - ECS Prod Update
 - Next Steps
 - Structure

