=========================================
Microservices with Docker, Flask & React
=========================================

:Description: using course to study Flask microservices
:Revision: 1.1
:Author: Mark Biggers <biggers@utsl.com>
:Ref: get, set environment secrets, https://github.com/theskumar/python-dotenv
:Last update: 06-03-2019 Mon @ 05:46:43 PM EDT


Course References
-----------------
References from the course-text...

 * *Docker for Python Developers* - https://mherman.org/presentations/dockercon-2018/#1

 * *Flask-SQLAlchemy* documentation - https://flask-sqlalchemy.palletsprojects.com/en/2.x/

 * *Testing Strategies in a Microservice Architecture* - https://martinfowler.com/articles/microservice-testing/

 * 


(?)

Introduction
------------
Working through the Course!


Preliminary work
----------------
Get the old Flask Udemy-course code working again...
