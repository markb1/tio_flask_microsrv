
# workon tio_flask_microsrv (OR)
# ln -s $VIRTUAL_ENV venv
# . venv/bin/activate

DCF = docker-compose
ENV = development

# want miniconda Py => Py 3.7!
run_create: MANUAL.TARGET.NO.GO
	ln -s services/users/Pipfile* .
	pipenv --python 3.7.4 install --dev # -r requirements.txt
	pipenv shell
	/bin/rm -f venv
	ln -s ~/.virtualenvs/tio_flask_microsrv-MkKu6203 venv

pipenv_update:
	pipenv --python 3.7.4 install --dev  # -r requirements.txt

run_build:
	${DCF} build users

run_users: run_db
	 ${DCF} up -d users; sleep 4

run_db:
	 ${DCF} up -d users-db; sleep 4
	 ${DCF} logs users-db | tail -10

run_shell:
	${DCF} exec users \
	  env FLASK_ENV=${ENV} FLASK_APP=${FLASK_APP} \
	  flask shell

run_tests: run_services
	 ${DCF} exec users python manage.py tests
	 ${DCF} exec users flake8 project

MGMT_CMD = recreate_db
run_manage: run_services
	 ${DCF} exec users python manage.py ${MGMT_CMD}

# \c users_dev
# \dt
run_psql: run_db
	${DCF} exec users-db psql -U postgres


# pipenv shell
# =>> http://localhost:5000/users/ping
ACTION=run
FLASK_APP=project/__init__.py
run_local:
	cd services/users; \
	env FLASK_ENV=${ENV} \
	  FLASK_APP=${FLASK_APP} \
	  DATABASE_URL="sqlite:////$$PWD/db/sqlite_dev.db" \
	python manage.py ${ACTION}

PDB = --pdb
TESTER = python manage.py test
TESTER = pytest -v -r fEP --tb=short ${PDB}

# ?? test_config.py, as in TIO course?
# make run_tests_local TESTER="python manage.py shell"
run_tests_local:
	cd services/users; \
	env FLASK_ENV=testing \
	  FLASK_APP=${FLASK_APP} \
	  DATABASE_TEST_URL="sqlite:///:memory:" \
	${TESTER}
	flake8 project

# run_tests_pg: run_db
# 	cd services/users; \
#  env SQLALCHEMY_TRACK_MODIFICATIONS=False \
#    DATABASE_TEST_URL=postgres://postgres:postgres@127.0.0.1:5432/users_test; \
#    ${TESTER}
